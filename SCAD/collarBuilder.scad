include <threads.scad>

$fn=100;

dxfFileName = "../DXF/screwTop.dxf";
layers =  ["0", "1", "2"];

diam = 55; // 52 on first attempt
rad = diam/2.;
wall = 4;
outerR = rad+wall;
threadHt = 15;
innerHt  = 40;
pit = 3;
topHt =  4;

module threadCollar(){
  difference(){
    cylinder(ht,outerR,outerR);
    metric_thread (diameter=diam, pitch=pit, length=threadHt, internal=true);
  }
}

//threadCollar();


module collar(){
 linear_extrude(innerHt)
    import (dxfFileName,layer= layers[0]);
  translate([0,0,-topHt])
    linear_extrude(topHt){
      import (dxfFileName,layer= layers[1]);
      import (dxfFileName,layer= layers[0]);
    }
  difference() {
    linear_extrude(threadHt)
      import (dxfFileName,layer= layers[2]);
    metric_thread (diameter=diam, pitch=pit, length=threadHt, internal=true);
  }
}
collar();

module ringBand(){
 linear_extrude(innerHt)
    import (dxfFileName,layer= layers[0]);
}
//ringBand();